import sys
import pandas as pd
import statistics as st
from scipy.stats import pearsonr
from EditCSV import EditCSV

bracket = [0, 190, 52, 32, 21, 101, 12, 100, 28, 54, 18, 93, 29, 43, 3, 152,
    2, 221, 40, 41, 30, 53, 15, 84, 25, 50, 5, 105, 22, 23, 7, 129, 1, 172,
    48, 27, 16, 26, 19, 79, 17, 39, 9, 110, 24, 36, 8, 134, 4, 185, 49, 44,
    10, 55, 11, 99, 14, 42, 13, 113, 33, 51, 6, 167]
teams = []
stats = []
avgs = []
adjAvgs = []
year = sys.argv[1]
#0 = Raw Points, 1 = Sagarin, 2 = Recent Sagarin,
#3 = Sagarin on LoBF of Raw Stats, 4 = Recent Sagarin on LoBF of Raw Stats
method = sys.argv[2]

TeamsCSV = EditCSV()
TeamsCSV.ReadCSV("Data/" + year + "/Teams.csv")
Teams = TeamsCSV.GetData()

StatsCSV = EditCSV()
StatsCSV.ReadCSV("Data/" + year + "/GameStats.csv")
Stats = StatsCSV.GetData()

#Using the team IDs in bracket to put together the teams list with their info
for id in bracket:
    for t in Teams:
        if (t[0] == str(id)):
            teams.append(t)
            break

#Setting up the stats list with empty lists
for i in range(0, len(teams)):
    stats.append([])
    avgs.append([])
    adjAvgs.append([])
    for j in range(0, 35):
        stats[i].append([])

#Gathering all of the game stats for each team in the bracket
for game in Stats:
    for i in range(0, len(bracket)):
        if (game[2] == str(bracket[i])):
            for j in range(6, 40):
                stats[i][j - 6].append(float(game[j]))

            rating = 0
            ratingType = 0
            homeAdv = 0

            if (method in [0, 1, 3]):
                ratingType = 4
                homeAdv = 3.13
            else:
                ratingType = 5
                homeAdv = 3.12

            teamFound = False
            for t in Teams:
                if (t[0] == game[3]):
                    if (game[5] == "N"):
                        rating = float(teams[i][ratingType]) - float(t[ratingType])
                    elif (game[5] == "H"):
                        rating = (float(teams[i][ratingType]) + homeAdv) - float(t[ratingType])
                    else:
                        rating = float(teams[i][ratingType]) - (float(t[ratingType]) + homeAdv)

                    stats[i][len(stats[i]) - 1].append(rating)
                    teamFound = True
                    break

            if (teamFound == False):
                nonD1Rating = 50

                if (game[5] == "N"):
                    rating = float(teams[i][ratingType]) - nonD1Rating
                elif (game[5] == "H"):
                    rating = (float(teams[i][ratingType]) + homeAdv) - nonD1Rating
                else:
                    rating = float(teams[i][ratingType]) - (nonD1Rating + homeAdv)

                stats[i][len(stats[i]) - 1].append(rating)

            break

#Calulating the average of each stat for each team
for i in range(0, len(stats)):
    for j in stats[i]:
        avgs[i].append(st.mean(j))

#Calculating the adjusted average of each stat for each team
for i in range(0, len(stats)):
    

sys.exit()

while len(teams) > 1:
    winners = []
    print("=============================================")
    print("Round of " + str(len(teams)))
    print("=============================================")

    for i in range(0, len(teams), 2):
        print(teams[i][1] + " - " + teams[i + 1][1])

sys.exit()

#Collecting the individual stats from each game for each team
teamStats = []
teamAdjStats = []
teamGameCount = 0
oppStats = []
oppAdjStats = []
oppGameCount = 0
for i in range(6, 40):
    teamStats.append([])
    teamAdjStats.append([])
    oppStats.append([])
    oppAdjStats.append([])

for i in range(0, Stats.shape[0]):
    if (Stats[2][i] == teamID):
        teamGameCount = teamGameCount + 1

        oppID = Stats[3][i]
        oppRat = 0

        for t in Teams:
            if (t[0] == oppID):
                oppRat = float(t[ratingType])
                break

        ratingMult = 0
        if (Stats[4][i] == 'H'):
            ratingMult = 100 - ((float(team[ratingType]) + 3.13) - oppRat)
        elif (Stats[4][i] == '@'):
            ratingMult = 100 - (float(team[ratingType]) - (oppRat + 3.13))
        else:
            ratingMult = 100 - (float(team[ratingType]) - oppRat)
        ratingMult = ratingMult / 100

        for j in range(6, 40):
            teamStats[j - 6].append(float(Stats[j][i]))
            teamAdjStats[j - 6].append(float(Stats[j][i]) * ratingMult)

    elif (Stats[2][i] == opp[0]):
        oppGameCount = oppGameCount + 1

        oppID = Stats[3][i]
        oppRat = 0

        for t in Teams:
            if (t[0] == oppID):
                oppRat = float(t[ratingType])
                break

        ratingMult = 0
        if (Stats[4][i] == 'H'):
            ratingMult = 100 - ((float(opp[ratingType]) + 3.13) - oppRat)
        elif (Stats[4][i] == '@'):
            ratingMult = 100 - (float(opp[ratingType]) - (oppRat + 3.13))
        else:
            ratingMult = 100 - (float(opp[ratingType]) - oppRat)
        ratingMult = ratingMult / 100

        for j in range(6, 40):
            oppStats[j - 6].append(float(Stats[j][i]))
            oppAdjStats[j - 6].append(float(Stats[j][i]) * ratingMult)

#Getting the average of each stat for each team
teamAvgs = []
for stat in teamStats:
    sum = 0
    for i in stat:
        sum = sum + i
    teamAvgs.append(sum / teamGameCount)

teamAdjAvgs = []
for stat in teamAdjStats:
    sum = 0
    for i in stat:
        sum = sum + i
    teamAdjAvgs.append(sum / teamGameCount)

oppAvgs = []
for stat in oppStats:
    sum = 0
    for i in stat:
        sum = sum + i
    oppAvgs.append(sum / oppGameCount)

oppAdjAvgs = []
for stat in oppAdjStats:
    sum = 0
    for i in stat:
        sum = sum + i
    oppAdjAvgs.append(sum / oppGameCount)

#Calculating line of best fit for each variable
teamB1s = []
teamB0s = []
oppB1s = []
oppB0s = []

for i in range(0, 34):
    teamB1s.append([])
    teamB0s.append([])
    oppB1s.append([])
    oppB0s.append([])

    for j in range(0, 34):
        y = teamStats[i]
        x = teamStats[j]
        corr = pearsonr(y, x)[0]
        b1 = corr * st.stdev(y) / st.stdev(x)
        b0 = st.mean(y) - b1 * st.mean(x)
        teamB1s[i].append(b1)
        teamB0s[i].append(b0)

        y = oppStats[i]
        x = oppStats[j]
        corr = pearsonr(y, x)[0]
        b1 = corr * st.stdev(y) / st.stdev(x)
        b0 = st.mean(y) - b1 * st.mean(x)
        oppB1s[i].append(b1)
        oppB0s[i].append(b0)

teamPredictions = []
oppPredictions = []

for i in range(0, 34):
    avgs = teamAdjAvgs
    if (i == 1 or i >= 18):
        avgs = oppAdjAvgs

    sum = 0

    for j in range(0, 34):
        sum = sum + ((avgs[j] * teamB1s[i][j]) + teamB0s[i][j])

    teamPredictions.append(sum / len(avgs))

for i in range(0, 34):
    avgs = oppAdjAvgs
    if (i == 1 or i >= 18):
        avgs = teamAdjAvgs

    sum = 0

    for j in range(0, 34):
        sum = sum + ((avgs[j] * oppB1s[i][j]) + oppB0s[i][j])

    oppPredictions.append(sum / len(avgs))

divList = []
for i in range(0, 34):
    divList.append("|")
statNames = ["Points", "OppPoints" , "FG", "FGA", "FGP", "3P", "3PA", "3PP", "FT",
    "FTA", "FTP", "ORB", "TRB", "AST", "STL", "BLK", "TOV", "PF", "OppFG", "OppFGA",
    "OppFGP", "Opp3P", "Opp3PA", "Opp3PP", "OppFT", "OppFTA", "OppFTP", "OppORB",
    "OppTRB", "OppAST", "OppSTL", "OppBLK", "OppTOV", "OppPF"]

df = pd.DataFrame(list(zip(statNames, divList, teamAvgs, teamAdjAvgs, teamPredictions, divList, oppAvgs, oppAdjAvgs, oppPredictions)),
    columns = ["", "|", "Avgs", "Adj Avgs", "Pred", "|", "Opp Avgs", "Opp Adj Avgs", "Opp Pred"])
print(df.to_string(index=False))

print("=====================================================================================")

print("Using predictions of just raw averages")
teamPredScore = (teamAvgs[0] + oppAvgs[1]) / 2
oppPredScore = (teamAvgs[1] + oppAvgs[0]) / 2
print(team[1] + ": " + str(teamPredScore) + "\n" + opp[1] + ": " + str(oppPredScore))

print("=====================================================================================")

print("Using predictions of just adjusted averages")
teamPredScore = (teamAdjAvgs[0] + oppAdjAvgs[1]) / 2
oppPredScore = (teamAdjAvgs[1] + oppAdjAvgs[0]) / 2
print(team[1] + ": " + str(teamPredScore) + "\n" + opp[1] + ": " + str(oppPredScore))

print("=====================================================================================")

print("Using predictions of putting adjusted averages onto LoBF of raw stats")
teamPredScore = (teamPredictions[0] + oppPredictions[1]) / 2
oppPredScore = (teamPredictions[1] + oppPredictions[0]) / 2
print(team[1] + ": " + str(teamPredScore) + "\n" + opp[1] + ": " + str(oppPredScore))

print("=====================================================================================")




















#
