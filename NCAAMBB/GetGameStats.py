from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import sys
from EditCSV import EditCSV

year = sys.argv[1]
CSVStats = EditCSV()
CSVStats.ReadCSV("Data/" + year + "/GameStats.csv")
CSVStats.ClearData()
CSVTeams = EditCSV()
CSVTeams.ReadCSV("Data/" + year + "/Teams.csv")
teams = CSVTeams.GetData()

URL1 = "https://www.sports-reference.com/cbb/schools/"
URL2 = "/" + year + "-gamelogs.html"
idCount = 0

for team in teams:
    teamID = team[0]
    SRName = team[2]
    print(URL1 + SRName + URL2)

    options = webdriver.ChromeOptions()
    options.add_argument("headless")
    browser = webdriver.Chrome(executable_path="C:/Program Files (x86)/Google/Chrome/chromedriver.exe", options=options)
    browser.get(URL1 + SRName + URL2)

    delay = 30
    try:
        myElem = WebDriverWait(browser, delay).until(EC.presence_of_element_located((By.ID, "sgl-basic")))
    except TimeoutException:
        print(SRName)
        print("Loading took too much time!")

    soup = BeautifulSoup(browser.page_source, 'html.parser')
    table = soup.find("table", {"id": "sgl-basic"})
    table_body = table.find('tbody')
    rows = table_body.find_all('tr')
    stats = []
    opps = []

    for row in rows:
        cols = row.find_all('td')

        if (len(cols) != 0):
            oppLink = str(list(cols)[2]).split('\"')
            if (len(oppLink) > 5):
                opps.append(oppLink[5].split('/')[3])
            else:
                opps.append("None")

            cols = [ele.text.strip() for ele in cols]
            stats.append([ele for ele in cols])

    oppCount = 0

    for row in stats:
        rowLen = len(row)

        date = row[0]
        opp = opps[oppCount]
        oppCount = oppCount + 1
        oppID = -1

        if (opp != "None"):
            for team in teams:
                if (opp == team[2]):
                    oppID = team[0]

        site = row[1]

        if (len(site) == 0):
            site = "H"

        result = row[3]

        if (len(result) > 1):
            result = "L"

        csvRow = []
        csvRow.append(idCount)
        idCount = idCount + 1
        csvRow.append(date)
        csvRow.append(teamID)
        csvRow.append(oppID)
        csvRow.append(site)
        csvRow.append(result)

        for i in range(4, 39):
            if (i != 22):
                csvRow.append(row[i])

        CSVStats.AddRow(csvRow)
    browser.quit()
CSVStats.WriteCSV("Data/" + year + "/GameStats.csv")





























#
