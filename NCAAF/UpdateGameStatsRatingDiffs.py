from EditCSV import EditCSV

CSVTeams = EditCSV()
CSVTeams.ReadCSV("Teams.csv")
CSVGames = EditCSV()
CSVGames.ReadCSV("Games.csv")
CSVStats = EditCSV()
CSVStats.ReadCSV("GameStats.csv")

for stats in CSVStats.data:
    for game in CSVGames.data:
        if (stats[1] == game[0]):
            ratingDiff = float(game[len(game) - 1])
            home = False
            homeTeam = game[4]
            awayTeam = game[5]
            homeRating = 0
            awayRating = 0

            if (stats[2] == homeTeam):
                home = True

            for team in CSVTeams.data:
                if (team[0] == homeTeam):
                    homeRating = float(team[len(team) - 1]) + 2.25

                if (team[0] == awayTeam):
                    awayRating = float(team[len(team) - 1])

            if (home):
                if (awayRating > homeRating):
                    ratingDiff = ratingDiff * -1
            else:
                if (homeRating > awayRating):
                    ratingDiff = ratingDiff * -1

            ratingDiff = round(ratingDiff, 2)

            CSVStats.AddData(stats[0], 0, str(ratingDiff), "RatingDiff")
            break

CSVStats.WriteCSV("GameStats.csv")
