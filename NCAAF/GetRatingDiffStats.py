import sys
from EditCSV import EditCSV

CSVGames = EditCSV()
CSVGames.ReadCSV("Games.csv")
CSVTeams = EditCSV()
CSVTeams.ReadCSV("Teams.csv")

Games = CSVGames.GetData()
Teams = CSVTeams.GetData()
LowerThreshold = float(sys.argv[1])
UpperThreshold = float(sys.argv[2])
NumGames = 0
NumUpsets = 0

for Game in Games:
    Home = Game[4]
    Away = Game[5]
    HomeScore = int(Game[6])
    AwayScore = int(Game[7])
    HomeRating = 0
    AwayRating = 0

    for Team in Teams:
        if (Home == Team[0]):
            HomeRating = float(Team[13]) + 2.25

        if (Away == Team[0]):
            AwayRating = float(Team[13])

    RatingDiff = abs(HomeRating - AwayRating)

    if ((RatingDiff <= UpperThreshold) & (RatingDiff >= LowerThreshold)):
        NumGames = NumGames + 1

        if (((HomeRating < AwayRating) & (HomeScore > AwayScore)) | ((HomeRating > AwayRating) & (HomeScore < AwayScore))):
            NumUpsets = NumUpsets + 1

UpsetPercent = float("{0:.2f}".format(((NumUpsets / NumGames) * 100)))

print("\n")
print("Number of games with a rating difference between " + str(LowerThreshold) + " and " + str(UpperThreshold) + ": " + str(NumGames))
print("Number of those games that were upsets: " + str(NumUpsets) + ", " + str(UpsetPercent) + "%")
print("\n")
