import sys
from EditCSV import EditCSV
import pandas as pd

team = str(sys.argv[1])
games = []
opponents = []
offenseStats = [6, 7, 8, 9, 10, 11, 14, 16, 22, 23, 24, 25, 26]
defenseStats = [6, 7, 8, 9, 10, 11, 14, 16, 22, 23, 24, 25, 26]

CSVGames = EditCSV()
CSVGames.ReadCSV("Games.csv")
CSVTeams = EditCSV()
CSVTeams.ReadCSV("Teams.csv")
CSVStats = EditCSV()
CSVStats.ReadCSV("GameStats.csv")

def getGames():
    for row in CSVStats.data:
        if (row[2] == team):
            games.append(row[1])

def getOpponents():
    for game in games:
        for stats in CSVStats.data:
            if ((stats[1] == game) & (stats[2] != team)):
                opponents.append(stats[2])
                break

def getTeamName(teamID):
    for t in CSVTeams.data:
        if (t[0] == teamID):
            return t[2]

def getTeamRating(teamID):
    for t in CSVTeams.data:
        if (t[0] == teamID):
            return t[len(t) - 1]

def getAllRawStatsDict(teamID, statsToGet):
    statsDict = dict()

    for row in CSVStats.data:
        if (row[2] == teamID):
            for stat in statsToGet:
                col = CSVStats.colNames[stat]

                if col in statsDict:
                    statsDict[col].append(float(row[stat]))
                else:
                    statsDict[col] = [float(row[stat])]

    return statsDict

def getGameRawStats(gameID, teamID, statsToGet):
    stats = []

    for row in CSVStats.data:
        if ((row[1] == gameID) & (row[2] == teamID)):
            for stat in statsToGet:
                stats.append(float(row[stat]))

            return stats

def getAdjStatsDict(statsDict):
    adjStatsDict = dict()
    ratingDiffs = statsDict["RatingDiff"]

    for stat in statsDict:
        if (stat != "RatingDiff"):
            col = statsDict[stat]
            adjCol = []
            i = 0

            for colItem in col:
                adjCol.append(round((1.0 - (ratingDiffs[i] / 100)) * colItem, 2))
                i = i + 1

            adjStatsDict[stat] = adjCol

    adjStatsDict["RatingDiff"] = ratingDiffs
    return adjStatsDict

def printStatsDict(statsDict):
    df = pd.DataFrame(statsDict)
    print(df)
    print("\nAverages")
    print(df.mean(axis = 0))
    print()

def printOffenseStats():
    print("\n========== OFFENSE ==========\n")

    #print("=== RAW STATS ===")
    offRawStats = getAllRawStatsDict(team, offenseStats)
    #printStatsDict(offRawStats)

    print("=== ADJUSTED STATS ===")
    offAdjStats = getAdjStatsDict(offRawStats)
    printStatsDict(offAdjStats)

def printDefenseStats():
    print("\n========== DEFENSE ==========\n")

    oppRawStats = []
    for i in range(0, len(games)):
        oppRawStats.append(getGameRawStats(games[i], opponents[i], defenseStats))

    defRawStats = dict()
    for i in range(0, len(defenseStats)):
        colName = CSVStats.colNames[defenseStats[i]]
        defRawStats[colName] = [oppRawStats[0][i]]

        for j in range(1, len(oppRawStats)):
            defRawStats[colName].append(oppRawStats[j][i])

    #print("=== RAW STATS ===")
    #printStatsDict(defRawStats)

    print("=== ADJUSTED STATS ===")
    defAdjStats = getAdjStatsDict(defRawStats)
    printStatsDict(defAdjStats)

print("\n" + getTeamName(team) + "\n" + getTeamRating(team))
getGames()
getOpponents()
printOffenseStats()
printDefenseStats()
