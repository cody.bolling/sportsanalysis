import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import sys
from EditCSV import EditCSV

#Variables for the teams and the current week
Teams = []
week = ""
if (len(sys.argv[1]) == 1):
    week = "0" + sys.argv[1]
else:
    week = sys.argv[1]

#Varaibles for CSV data reading and writing
CSVGames = EditCSV()
CSVGames.ReadCSV("Games.csv")
CSVTeams = EditCSV()
CSVTeams.ReadCSV("Teams.csv")
CSVStats = EditCSV()
CSVStats.ReadCSV("GameStats.csv")

def formatDate(date):
    dateList = date.split()

    switcher = {
        "January": "01",
        "February": "02",
        "March": "03",
        "April": "04",
        "May": "05",
        "June": "06",
        "July": "07",
        "August": "08",
        "September": "09",
        "October": "10",
        "November": "11",
        "December": "12"
    }

    monthNum = switcher.get(dateList[1])
    dayNum = dateList[2][0:2]
    return monthNum + "/" + dayNum + "/" + dateList[3]

def teamDuplicate(Team):
    for team in Teams:
        if (team == Team):
            return True

    return False

def getGameStats(URL, gameID, homeID, awayID):
    print("Adding game stats for game " + str(gameID))
    gameURL = URL + "/team-stats"

    options = webdriver.ChromeOptions()
    options.add_argument("headless")
    browser = webdriver.Chrome(options=options)
    browser.get(gameURL)

    delay = 30
    try:
        myElem = WebDriverWait(browser, delay).until(EC.presence_of_element_located((By.CLASS_NAME, "gamecenter-team-stats-table")))
    except TimeoutException:
        print("Loading took too much time!")

    soup = BeautifulSoup(browser.page_source, 'html.parser')
    stats = []
    table = soup.find("table", {"class": "gamecenter-team-stats-table"})
    table_body = table.find('tbody')
    rows = table_body.find_all('tr')

    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        stats.append([ele for ele in cols if ele])

    for i in range(0, 3, 2):
        ID = len(CSVStats.data)
        teamID = ""
        if (i == 0):
            teamID = awayID
        else:
            teamID = homeID

        CSVStats.AddRow([str(ID), str(gameID), str(teamID), stats[1][i], stats[2][i],
                stats[3][i], stats[4][i], stats[5][i], stats[7][i], stats[8][i],
                stats[9][i], stats[10][i], stats[15][i].split("-", 1)[0],
                stats[15][i].split("-", 1)[1], stats[16][i].split("-", 1)[0],
                stats[16][i].split("-", 1)[1], stats[17][i].split("-", 1)[0],
                stats[17][i].split("-", 1)[1], stats[19][i].split("-", 1)[0],
                stats[19][i].split("-", 1)[1], stats[20][i].split("-", 1)[0],
                stats[20][i].split("-", 1)[1], stats[22][i].split("-", 1)[1],
                stats[22][i].split("-", 1)[0], stats[23][i].split("-", 1)[1],
                stats[23][i].split("-", 1)[0], "0"])

    browser.quit()

def getGameData(URL, subdivision):
    page = requests.get(URL)
    soup = BeautifulSoup(page.content, "html.parser")
    html = list(list(list(list(list(list(list(list(list(list(list(list(soup.children)[2])[3])[3])[1])[3])[3])[1])[9])[3])[1])[1])
    ID = len(CSVGames.data)

    for i in range(1, len(html), 2):
        date = formatDate(list(list(html[i])[1])[0])
        games = list(list(html[i])[3])

        for j in range(1, len(games), 2):
            print("Getting game data for game " + str(ID))
            game = list(list(list(games[j])[1])[7])

            if (len(list(list(game[1])[7])) == 0 | len(list(list(game[3])[7])) == 0):
                continue

            awayTeam = list(list(game[1])[5])[0]
            awayScore = list(list(game[1])[7])[0]
            homeTeam = list(list(game[3])[5])[0]
            homeScore = list(list(game[3])[7])[0]
            href = str(list(games[j])[1]).split()[2]
            gameURL = "https://www.ncaa.com" + href[6:len(href) - 2]

            awayID = CSVTeams.GetValue(awayTeam.lower().strip(), "NCAAName", "ID")
            homeID = CSVTeams.GetValue(homeTeam.lower().strip(), "NCAAName", "ID")

            if ((awayID != "null") & (homeID != "null") & (teamDuplicate(awayTeam) == False) & (teamDuplicate(homeTeam) == False) & (gameURL != "https://www.ncaa.com/game/3785935")):
                CSVGames.AddRow([str(ID), str(subdivision), str(sys.argv[1]), str(date), str(homeID), str(awayID), str(homeScore), str(awayScore), str(gameURL), "0"])
                print(gameURL)
                getGameStats(gameURL, ID, homeID, awayID)
                Teams.append(awayTeam)
                Teams.append(homeTeam)
                ID += 1

FBS_URL = "https://www.ncaa.com/scoreboard/football/fbs/2019/" + week + "/all-conf"
FCS_URL = "https://www.ncaa.com/scoreboard/football/fcs/2019/" + week + "/all-conf"

getGameData(FBS_URL, "FBS")
getGameData(FCS_URL, "FCS")

CSVGames.WriteCSV("Games.csv")
CSVStats.WriteCSV("GameStats.csv")
