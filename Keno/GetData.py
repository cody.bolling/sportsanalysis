from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import sys
from EditCSV import EditCSV
import pandas as pd
import matplotlib.pyplot as plt

#CSV = EditCSV()
#CSV.ReadCSV("TeamStats.csv")
URL = "https://www.masslottery.com/games/lottery/search/results-history.html?game_id=15&mode=1&selected-year=2019&selected-month=12&year=2019&month=12&min=2188764&end_number_method=1&max=2198063"

options = webdriver.ChromeOptions()
options.add_argument("headless")
browser = webdriver.Chrome(options=options)
browser.get(URL)

delay = 60
try:
    myElem = WebDriverWait(browser, delay).until(EC.presence_of_element_located((By.CLASS_NAME, "odd")))
except TimeoutException:
    print("Loading took too much time!")

soup = BeautifulSoup(browser.page_source, 'html.parser')
games = []
numberFreq = []
table = soup.find("table", {"class": "zebra-body-only"})
table_body = table.find('tbody')
rows = table_body.find_all('tr')

for row in rows:
    cols = row.find_all('td')
    cols = [ele.text.strip() for ele in cols]
    games.append([ele for ele in cols if ele])

for i in range(0, 80):
    numberFreq.append(0)

games.remove([])

allNumbers = []

for game in games:
    numbers = game[2].split('-')

    for num in numbers:
        allNumbers.append(int(num))
        numberFreq[int(num) - 1] = numberFreq[int(num) - 1] + 1

#print(allNumbers)
df = pd.DataFrame(allNumbers)
ax = df.plot.hist(bins = 80)
ax.plot()
plt.show()
